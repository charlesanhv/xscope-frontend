'use client'
import { LIST_DATA_TWITTER } from '@/constants'
import Image from 'next/image'
import React, { useMemo, useState } from 'react'
import Link from 'next/link'

interface HistoryProps {
    itemActive: any;
}

const History: React.FC<HistoryProps> = ({ itemActive }) => {
    const [dataHistory, setDataHistory] = useState(LIST_DATA_TWITTER?.slice(0, 5))
    const handleClearHistory = () => {
        setDataHistory([])
    }

    const dataRenderHistory = useMemo(() => {
        let data = [...dataHistory]
        if (itemActive && !data.some(item => item.slug === itemActive.slug)) {
            data = [...data, itemActive]
        }
        return data;
    }, [itemActive, dataHistory])

    return (
        <div className={`w-full max-w-[342px] flex flex-col gap-4 relative ${dataHistory?.length < 7 ? null : 'history'}`}>
            <div className="flex items-center justify-between">
                <span className="text-[#C5D8EE] font-[400] text-[14px] tracking-[-0.14px] opacity-60">History</span>
                <span className="text-[#f5f5f599] font-[300] text-[14px] tracking-[0.28px] cursor-pointer hover:text-[#C5D8EE] hover:opacity-60" style={{ transition: 'all 0.3s ease' }} onClick={() => handleClearHistory()}>Clear all</span>
            </div>
            <div className="flex flex-col gap-1 max-h-[564px] overflow-y-scroll relative listHistory">
                {dataRenderHistory?.map((item, index) => {
                    return (
                        <React.Fragment key={index}>
                            {item?.slug === itemActive?.slug ? (
                                <div key={index} className="flex gap-4 items-start py-3 pl-3 pr-6 cursor-pointer w-full rounded-[8px] itemHistoryActive" >
                                    <Image src={item?.logo} alt={item?.user_name} width={28} height={28} className="rounded-[28px]" />
                                    <div className="flex flex-col">
                                        <span className="text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6] overflow-hidden text-base line-clamp-1">{item?.name}</span>
                                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]">@{item?.user_name}</span>
                                    </div>
                                </div>
                            ) : (
                                <Link href={`/app/${item?.slug}?time=3`} className="flex gap-4 items-start p-3 cursor-pointer w-full rounded-[8px] item_list_twit" >
                                    <Image src={item?.logo} alt={item?.user_name} width={28} height={28} className="rounded-[28px]" />
                                    <div className="flex flex-col">
                                        <span className="text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6] overflow-hidden text-base line-clamp-1">{item?.name}</span>
                                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]">@{item?.user_name}</span>
                                    </div>
                                </Link>
                            )
                            }
                        </React.Fragment>
                    )
                })}
            </div>
        </div>
    );
}

export default History
