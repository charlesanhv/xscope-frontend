'use client'
import Image from 'next/image'
import '../style/App.css'
import React, { useState } from 'react'
import bg_content_item from '../../../public/images/App/bg_content_item.png'
import IconDropdown from '../../../public/images/App/icon_dropdown.svg'
import parse from 'html-react-parser'

interface ContentItemProps {
    itemActive: any,
    timeRange: number
}

import {
    Dialog,
    DialogTrigger,
} from "@/components/ui/dialog"
import { Button } from "@/components/ui/button"
import ModalRange from './ModalRange'
import { archivo } from '@/app/font'


const ContentItem: React.FC<ContentItemProps> = ({ itemActive, timeRange }) => {

    const [timerange, setTimerange] = useState(timeRange)

    const handleChangeTimeRange = (value: any) => {
        setTimerange(value)
    }
    return (
        <div className='flex-1 w-full h-full relative p-6 '>
            <Image src={bg_content_item} alt='bg_content_item' className='w-full h-full absolute top-0 left-0 right-0 bottom-0' />
            <div className='pt-6 pr-4 pl-3 flex items-start justify-between z-200'>
                <div className='flex items-start gap-3 max-w-[453px]'>
                    <Image src={itemActive?.logo} alt={itemActive?.user_name || ''} width={40} height={40} className="rounded-[40px]" />
                    <div className="flex flex-col">
                        <span className="text-[20px] font-[300] tracking-[-0.2px] text-[#fafafae6] overflow-hidden text-base line-clamp-1">{itemActive?.name}</span>
                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafae6] opacity-60">@{itemActive?.user_name}</span>
                    </div>
                </div>
                <div className='flex gap-3 flex-col justify-end items-end'>
                    <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#C5D8EE] opacity-60'>Time range</span>
                    <Dialog>
                        <DialogTrigger asChild >
                            <Button variant="outline" className="h-[32px] pr-3 pl-2 border-[1px] border-[#998dc64d] bg-transparent rounded-[4px] hover:bg-transparent z-50 flex gap-2 items-center">
                                <Image src={IconDropdown} alt='IconDropdown' />
                                <span className='text-[16px] font-[300] tracking-[0.32px] text-[#F5F5F5]'>{timerange}D</span>
                            </Button>
                        </DialogTrigger>
                        <ModalRange itemSelected={timeRange} changeLink={false} onChange={handleChangeTimeRange} />
                    </Dialog>
                </div>
            </div>
            <div className=" pt-[40px] pl-[12px]">
                <div className={`${archivo.className} max-h-[438px] pr-[56px] text-[16px] font-[300] tracking-[0.32px] text-[#fafafae6] relative div_content overflow-y-scroll`} style={{ zIndex: 50 }}>{parse(itemActive?.content ?? '')}</div>
            </div>
        </div>
    );
}

export default ContentItem
