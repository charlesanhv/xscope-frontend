'use client'
// import { ModeToggle } from '@/components/mode-toggle'
import Link from 'next/link'
export default function Footer() {
  return (
    <div className='h-[124px] bg-[#151515] z-50 relative px-[72px]'>
      <div className='w-full max-w-[1222px] h-[124px] flex justify-between items-center mx-auto'>
      <div className='flex justify-between w-max items-center gap-3'>
        <span className='text-[22px] font-[700] tracking-[-0.22px] text-[#FAFAFAE6]'>Footer</span>
        <div className='w-px h-5 mt-1 ml-2' style={{ background: 'rgba(255, 255, 255, 0.08)' }} />
        <div className='flex items-center gap-[6px]'>
          <Link href='https://twitter.com/home' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>@Twitter</Link>
          <Link href='https://twitter.com/home' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>@Telegram</Link>
        </div>
      </div>
      <span className='text-[14px] font-[250] tracking-[-0.14px] px-2 text-[#FAFAFAE6]'>Powered by <strong className='font-[400]'>Statistr</strong></span>
      </div>
    </div>
  )
}
