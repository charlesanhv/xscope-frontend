'use client'
import { useAppContext } from '@/app/app-provider'
import './style/header.css'
// import ButtonLogout from '@/components/button-logout'
import Image from 'next/image'
// import { ModeToggle } from '@/components/mode-toggle'
import Link from 'next/link'
import Logo from '../../public/images/Header&Footer/logoXscope.svg'
import DummyUser from '../../public/images/Header&Footer/DummyUser.svg'
import Tab_name from '../../public/images/Home/Tab_name.png'

export default function Header() {
  const dummyDataUser = {
    credits: 212,
    nameuser: 'Trann Son',
    avatar: DummyUser
  }
  const { user } = useAppContext()

  return (
    <div
      className='fixed inset-x-0 top-0 left-0 h-16 px-20  bg-bgheader z-[900]'
      style={{ background: 'linear-gradient(180deg, #171617 0%, #37373E 100%)' }}
    >
      <div className='max-w-[1222px] h-full mx-auto flex items-center justify-between'>
        <div className='flex justify-between w-max items-center gap-3'>
          <Link href='/' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
            {' '}
            <Image src={Logo} alt='logo' />
          </Link>

          <div className='w-px h-5 mt-1 ml-2' style={{ background: 'rgba(255, 255, 255, 0.08)' }} />
          <div className='flex items-center gap-[6px]'>
            <Link href='/features' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
              Features
            </Link>
            <Link href='/about' className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
              About
            </Link>
          </div>
        </div>
        <div>
          {user ? (
            <div className='flex items-center gap-2'>
              <div className='h-[38px] flex items-center gap-2 header-info-user w-full relative'>
                <div className='flex items-center gap-2 w-full h-full rounded-[6px] px-3'>
                  <div className='w-[20px] h-[20px] rounded-[20px] bg-[#666666]' />
                  <span className='text-[14px] font-[400] tracking-[-0.14px] text-[#FAFAFAE6]'>
                    {dummyDataUser?.credits}
                  </span>
                  <span className='text-[14px] font-[300] tracking-[-0.28px] text-[#FAFAFAE6] opacity-50'>Credits</span>
                </div>
                <Image src={Tab_name} alt='Tab_name' className='absolute top-0 right-0 left-0' height={38} />
              </div>
              <Image
                src={dummyDataUser?.avatar}
                alt='logo'
                width={38}
                height={38}
                className='b+rounded-[38px] border-[1px] border-[#ffffff3d]'
              />
            </div>
          ) : (
            <Link href={'/SignIn'} className='text-[16px] font-[400] tracking-[-0.16px] px-2 text-[#FAFAFAE6]'>
              Sign In
            </Link>
          )}
        </div>
      </div>
      {/* <ModeToggle /> */}
    </div>
  )
}
