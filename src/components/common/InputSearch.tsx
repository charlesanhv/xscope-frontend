'use client'
// import { Metadata } from "next";
import React from "react";
import Image from "next/image";
import IconSearchInput from '../../../public/images/App/icon_search_input.svg'
import LightingSearchHover from '../../../public/images/App/Lighting_search_hover.svg'
import LightingSearchActive from '../../../public/images/App/Lighting_search_active.svg'
import '../style/InputSearch.css'
import IconButtonCmd from '../../../public/images/App/icon_button_cmd.svg'
import Link from "next/link";

interface InputSearchProps {
    valueSearch: string;
    onChange: any,
    changeLink: boolean,
    link: string,
    placeholder: string
}


const InputSearch: React.FC<InputSearchProps> = ({ valueSearch = '', onChange, changeLink = false, link = '', placeholder = "Search by user name ex @rolex" }) => {
    return (
        <>
            {changeLink ? (
                <Link href={link} className="w-full px-24px h-[56px] inputSearch cursor-pointer" >
                    <div className="w-full flex items-center gap-3 justify-center h-full relative" style={{ zIndex: 50 }}>
                        <Image src={IconSearchInput} alt='IconSearch' />
                        <input placeholder={placeholder} onChange={(e) => onChange && onChange(e.target.value)} value={valueSearch} className="outline-none bg-transparent border-none h-full w-[66px] cursor-pointer" />
                        {valueSearch ? null : <Image src={IconButtonCmd} alt='IconButtonCmd' />}
                    </div>
                    <Image src={LightingSearchHover} alt='LightingSearchHover' className="absolute w-full top-[-13px] left-0 right-0 bottom-0 h-[82px] lightingSearchHover" />
                    <Image src={LightingSearchActive} alt='LightingSearchActive' className="absolute w-full top-[-13px] left-0 right-0 bottom-0 h-[82px] LightingSearchActive" />                </Link>
            ) : (
                <div className="w-full px-24px h-[56px] inputSearch" >
                    <div className="w-full flex items-center gap-3 justify-center h-full relative" style={{ zIndex: 50 }}>
                        <Image src={IconSearchInput} alt='IconSearch' />
                        <input placeholder={placeholder} onChange={(e) => onChange && onChange(e.target.value)} value={valueSearch} className="outline-none text-center bg-transparent border-none h-full w-[280px] text-[#F5F5F5] font-[16px] font-[300] tracking-[-0.16px]" />
                        {valueSearch ? null : <Image src={IconButtonCmd} alt='IconButtonCmd' />}
                    </div>
                    <Image src={LightingSearchHover} alt='LightingSearchHover' className="absolute w-full top-[-13px] left-0 right-0 bottom-0 h-[82px] lightingSearchHover" />
                    <Image src={LightingSearchActive} alt='LightingSearchActive' className="absolute w-full top-[-13px] left-0 right-0 bottom-0 h-[82px] LightingSearchActive" />
                </div>
            )}
        </>
    );
}

export default InputSearch
