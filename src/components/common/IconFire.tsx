'use client'
// import { Metadata } from "next";
import React from "react";
import Image from "next/image";
import IconFireImg from '../../../public/images/Home/icon_fire.svg'
import IconFireHoverImg from '../../../public/images/Home/icon_fire_hover_2.png'
import '../style/IconFire.css'

const IconFire: React.FC = () => {
    return (
        <div className="flex flex-col items relative hoverIcon ml-[-8px]" style={{zIndex:300}}>
            <div className="flex items-center justify-center w-[36px] h-[36px]">
                <Image src={IconFireImg} alt="logo" className="fireIcon" width={24} height={24}/>
            </div>
            <Image src={IconFireHoverImg} alt="logo" width={34} height={34} className="fireHoverIcon" />
            <div className="bg-[#232323] px-3 h-[24px] absolute top-[40px] left-[-32px] text-[12px] font-[400] tracking-[0.24px] text-[#F5F5F5] w-[102px] flex items-center justify-center rounded-[4px] popupText">Hot Profile</div>
        </div>
    );
}

export default IconFire
