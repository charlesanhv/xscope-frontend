import Logo from '../../public/images/App/logo_twitter_dummy.svg'

export const LIST_DATA_TWITTER = [
    {
        id:1,
        name:'Statistr | Hiring Data Contributor',
        user_name:'statistr_',
        slug:'statistr-hiring-data-contributor',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:2,
        name:'Statistrek_ Megamall',
        user_name:'statistrek.m',
        slug:'statistrek-megamall',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:3,
        name:'Statist Cookies',
        user_name:'statistr_s.cookies',
        slug:'statistr-cookies',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:4,
        name:'Yung Patek',
        user_name:'louis.vertical',
        slug:'louis-vertical',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:5,
        name:'Travis Nguyen',
        user_name:'travis.sconguyen',
        slug:'travis-sconguyen',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:6,
        name:'PoP Btc Magazine',
        user_name:'the_beck',
        slug:'the-beck',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:7,
        name:'Don Toliver',
        user_name:'don_toliver',
        slug:'don-toliver',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:8,
        name:'Travis JBec',
        user_name:'travis_jbec',
        slug:'travis-jbec',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:9,
        name:'Mad Meloodic',
        user_name:'mad_meloodic',
        slug:'mad-meloodic',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
    {
        id:10,
        name:'Kiuchangyie',
        user_name:'kiuchangyie',
        slug:'kiuchangyie',
        logo:Logo,
        content:`
        <p>📢 3 days Recap with @Statistr 📢</p>
        <p>1.Ethereum's London Upgrade: Vitalik dives deep into the impact of EIP-1559 and the progress towards Ethereum 2.0. </p>
        <p>2.NFT Boom Continues: Insights on the latest developments in the NFT space and Ethereum's role in shaping its future.</p>
        <p>3.DeFi Evolution: Vitalik discusses the evolving landscape of decentralized finance, highlighting challenges and opportunities.</p>
        <p>4.Layer 2 Solutions: Updates on Ethereum's scaling efforts with layer 2 solutions and the quest for greater throughput.</p>
        <p>5.Community AMA: Recap of Vitalik's community Ask Me Anything session, covering a wide range of topics from governance to technical advancements.</p>
        <p>6.Sustainability Initiatives: Ethereum's journey towards environmental sustainability and plans to reduce its carbon footprint.</p>
        <p>7.Future of Ethereum: Insights into Vitalik's vision for Ethereum's future and its role in shaping the decentralized web.</p>
        8.Stay tuned for more updates from the forefront of blockchain innovation! #Ethereum #Crypto #DeFi`
    },
]