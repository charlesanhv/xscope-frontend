import { Unbounded, Archivo } from 'next/font/google'
 
export const unbounded = Unbounded({
  subsets: ['latin'],
  display: 'swap',
})
 
export const archivo = Archivo({
  subsets: ['latin'],
  display: 'swap',
})