import { LIST_DATA_TWITTER } from '@/constants'
import '../../../components/style/App.css'
import React from "react";
// import Link from "next/link";
import History from "@/components/app/History";
import ContentItem from "@/components/app/ContentItem";
import InputSearch from '@/components/common/InputSearch';
export default async function UIAppItem({ params, searchParams }: any) {
    const timeRange = searchParams?.time ?? 3

    const itemSelected = LIST_DATA_TWITTER?.find((item) => item?.slug === params.slug)

    return (
        <main className="flex h-screen pt-[136px] px-[72px]" style={{ background: 'linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}>
            <div className="w-full max-w-[1222px] mx-auto flex flex-col gap-10">
                <InputSearch valueSearch={''} changeLink={true} link={'/app'} placeholder={'Search'} onChange={null} />
                <div className="flex items-start gap-10">
                    <History itemActive={itemSelected} />
                    <ContentItem timeRange={timeRange} itemActive={itemSelected} />
                </div>
            </div>
        </main >
    );
}
