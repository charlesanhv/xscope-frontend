'use client'
// import { Metadata } from "next";
import { useState } from "react";
import Image from "next/image";
import IconSearch from '../../../public/images/App/icon_search.svg'
import '../../components/style/App.css'
import { LIST_DATA_TWITTER } from "@/constants";
import ModalRange from "@/components/app/ModalRange";
import {
    Dialog,
    DialogTrigger,
} from "@/components/ui/dialog"
import { archivo } from "../font";
import Logo from '../../../public/images/App/logo_twitter_dummy.svg'
import IconAdd from '../../../public/images/App/icon_add.svg'
import InputSearch from "@/components/common/InputSearch";
import IconFire from "@/components/common/IconFire";
import { AnimatePresence, motion } from 'framer-motion'

const dummy_suggest_list = [
    {
        id: 1,
        name: 'Travis JBec',
        hot: true
    },
    {
        id: 2,
        name: 'Mad Meloodic',
        hot: true
    },
    {
        id: 3,
        name: 'Thebec',
        hot: false
    },
    {
        id: 4,
        name: 'Don Toliver',
        hot: false
    },
    {
        id: 5,
        name: 'Dang Beo',
        hot: false
    },
    {
        id: 6,
        name: 'Kiuchangyie',
        hot: false
    },
    {
        id: 7,
        name: 'Playboi Carti',
        hot: false
    },
]

export default function UIApp() {
    const [valueSearch, setValueSearch] = useState('')
    const [itemSelect, setItemSelect] = useState('')

    // motion

    const spring = {
        // type: 'spring',
        // damping: 10,
        // stiffness: 100
        ease: 'easeInOut',
        duration: 0.5
    }


    return (
        <div className="flex h-screen py-2" style={{ background: 'linear-gradient(111deg, #46454E 0%, #1B1B1C 33.33%, #161616 50.5%, #211E33 100%)' }}>
            <div className="w-full max-w-[800px] mx-auto pt-[214px] flex flex-col gap-[40px] justify-start items-center">
                <span className="bg-clip-text text-[32px] font-[300] tracking-[-0.32px]" style={{ background: 'linear-gradient(90deg, rgba(239, 239, 239, 0.90) 0%, rgba(254, 254, 254, 0.90) 49.5%, rgba(239, 239, 239, 0.88) 100%)', WebkitBackgroundClip: 'text', WebkitTextFillColor: 'transparent' }}>Start here</span>
                <InputSearch valueSearch={valueSearch} onChange={(value: string) => setValueSearch(value)} changeLink={false} link={''} placeholder={'Search by user name ex @rolex'} />
                <AnimatePresence>
                    {valueSearch ? (
                        <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={spring}>
                            <Dialog >
                                <div className="flex flex-col pl-[80px] w-[512px]">
                                    {LIST_DATA_TWITTER?.filter((item) => {
                                        return item?.name?.toLowerCase().includes(valueSearch?.toLowerCase())
                                    })?.map((item: any, index: any) => {
                                        return (
                                            <DialogTrigger asChild key={index}>
                                                <div key={index} className="flex gap-4 items-start p-3 cursor-pointer w-full item_list_twit" onClick={() => setItemSelect(item?.slug)}>
                                                    <Image src={item?.logo} alt={item?.user_name} width={32} height={32} className="rounded-[32px]" />
                                                    <div className="flex flex-col">
                                                        <span className="text-[16px] font-[300] tracking-[-0.16px] text-[#fafafae6]">{item?.name}</span>
                                                        <span className="text-[16px] font-[300] tracking-[0.32px] text-[#fafafa99]">@{item?.user_name}</span>
                                                    </div>
                                                </div>
                                            </DialogTrigger>
                                        )
                                    })}
                                </div>
                                <ModalRange itemSelected={itemSelect} />
                            </Dialog>
                        </motion.div>
                    ) : (
                            <div className="flex flex-wrap gap-2 justify-center max-w-[612px] mx-auto">
                                {dummy_suggest_list?.map((item, index) => {
                                    return (
                                        <div key={index} className="flex items-center gap-[8px] h-[40px] w-max px-4 border border-[#998dc652] rounded-[20px] cursor-pointer suggest_item" style={{ paddingRight: item?.hot ? '8px' : '16px', zIndex: 200 }} onClick={() => { setValueSearch(item?.name) }}>
                                            <Image src={IconSearch} alt='IconSearch' className="iconSearch" width={16} height={16} />
                                            <Image src={IconAdd} alt='IconSearch' className="iconAdd" width={16} height={16} />
                                            <Image src={Logo} width={22} height={22} alt='logo' className="rounded-[22px] " />
                                            <span className={`${archivo.className} text-[14px] font-[400] tracking-[0.28px] text-[#F5F5F5]`}>{item?.name}</span>
                                            {item?.hot ? <IconFire /> : null}
                                        </div>
                                    );
                                })}
                            </div>
                    )}
                </AnimatePresence>
            </div>
        </div >
    );
}
